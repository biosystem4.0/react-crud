import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

const url="http://34.67.132.201:8080/RestQuidProAlfA/services/bio/validaUsuario";
        // http://34.67.132.201:8080/RestQuidProAlfA/services/bio/validaUsuario

          

class App extends Component {
  state={
  data:[],
  usuario:{
    usuario:'',
    password:'',
    valido: false,
    activo: false
  }

}

peticionGet=()=>{
axios.get(url).then(response=>{
  this.setState({data: response.data});
}).catch(error=>{
  console.log(error.message);
})

}

peticionPost=async()=>{
 await axios.post(url,this.state.usuario).then(response=>{
 console.log("funciona todo bien " + response.data);
  }).catch(error=>{
    console.log(error.message);
  })
}

validarUsuario=(usuario)=>{
  this.setState({
    tipoModal: 'actualizar',
    usuario: {
      usuario: usuario.usuario,
    password: usuario.password,
    valido: usuario.valido,
    activo: usuario.activo
    }
  })
}

handleChange=async e=>{
e.persist();
await this.setState({
  usuario:{
    ...this.state.usuario,
    [e.target.name]: e.target.value
  }
});
console.log(this.state.usuario);
}

 // componentDidMount() {
 //   this.peticionPost();
 // }
  

  render(){
    const {usuario}=this.state;
  return (
    <div className="App">
    <br /><br /><br />
  <button className="btn btn-success" onClick={()=>{this.setState({usuario: null})}}>Limpiar Usuario</button>
  <br /><br />
    <table className="table">
      <thead>
         <tr> 
           <th></th>
          <th>Nombre</th>
          <th>Usuario</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {this.state.data.map(usuario=>{
          return(
            <tr>
             
          <td>{usuario.usuario}</td>
          <td>{usuario.password}</td>
         
          <td>
                <button className="btn btn-primary" onClick={()=>{this.validarUsuario(usuario); this.peticionPost()}}><FontAwesomeIcon icon={faEdit}/></button>
                {"   "}
                </td>
          </tr>
          )
        })}
      </tbody>
    </table>



    <Modal isOpen={true}>
               
                <ModalBody>
                  <div className="form-group">
                    <label htmlFor="usuario">Usuario</label>
                    <input className="form-control" type="text" name="usuario" id="usuario_id" onChange={this.handleChange} value={this.state.usuario.usuario}/>
                    <br />
                    <label htmlFor="nombre">Password</label>
                    <input className="form-control" type="text" name="password" id="password_id" onChange={this.handleChange} value={this.state.usuario.password}/>
                    </div>
                </ModalBody>

                <ModalFooter>
                  <button className="btn btn-secondary" onClick={()=>this.peticionPost(this.state.usuario)}>
                    Actualizar
                  </button>
                </ModalFooter>
          </Modal>
  </div>



  );
}
}
export default App;
